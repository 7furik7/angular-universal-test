import { Injectable } from "@angular/core";
import { delay } from "rxjs/operators";
import { of } from "rxjs";
import { makeStateKey, TransferState } from "@angular/platform-browser";
import { Store } from "@ngrx/store";
import { SetStateFeature } from "./app.reducer";



@Injectable()
export class AppInitService {
    constructor(
        private store: Store<any>,
        private ts: TransferState
    ) {}

    preload() {


        this.store.dispatch(new SetStateFeature( {cat: 'http://r.ddmcdn.com/s_f/o_1/cx_0/cy_157/cw_1327/ch_1327/w_720/APL/uploads/2013/01/smart-cat-article.jpg'} ));

        return new Promise((resolve, reject) => {
            of(1).pipe(
                delay(5000)
            ).subscribe(data => {
                console.log('subscribe', data);
                resolve(data);
            });
        })
    }
}


export function appIniProviderFactory(provider: AppInitService) {
    return () => provider.preload();
  }