import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { FetchText } from './app.reducer';
import { selectAppName, selectAppConfig, selectCat } from './app.selectors';
import { delay, map, switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public app: Observable<any>;
  public cat$: Observable<string>;
  constructor(
    private store: Store<any>
  ){

  }
  ngOnInit(){
    this.store.dispatch(new FetchText());
    this.app = this.store.select(selectAppConfig); 
    this.cat$ = this.store.select(selectCat);  

  }


  reset(){
    this.store.dispatch({type: 'RESET'});
  }
}
