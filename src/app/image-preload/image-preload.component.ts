import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-image-preload',
  templateUrl: './image-preload.component.html',
  styleUrls: ['./image-preload.component.scss']
})
export class ImagePreloadComponent implements OnInit {
  @Input() image: string;
  public src: string;
  constructor(
    private cd: ChangeDetectorRef
  ) { 
  }

  ngOnInit() {
    if(!this.src){
      
    }
    setTimeout(()=>{
      this.src = this.image;
    }, 2000)
  }

}
