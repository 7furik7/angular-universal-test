import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { FETCH_TEXT, FetchText, FetchTextSuccess, CAT_KEY } from './app.reducer';
import { Observable, of } from 'rxjs';
import { map, switchMap, delay } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { TransferState } from '@angular/platform-browser';



const HTTPCAT = 'http://r.ddmcdn.com/s_f/o_1/cx_0/cy_157/cw_1327/ch_1327/w_720/APL/uploads/2013/01/smart-cat-article.jpg';
@Injectable()
export class AppEffects {

  constructor(
    private actions$: Actions,
    private ts: TransferState
  ) {}


  @Effect()
  fetchText: Observable<Action> = this.actions$.pipe(
    ofType(FETCH_TEXT),
    switchMap(_ => {
      const cat = this.ts.get(CAT_KEY, null as any);
      if(cat){
        console.log('EFFETS - cat is from server');
        return of(new FetchTextSuccess(cat) );
      }
      this.ts.set(CAT_KEY, HTTPCAT);
      return of(new FetchTextSuccess(HTTPCAT) );
      
    })

  )
}
