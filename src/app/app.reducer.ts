import { Action } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { makeStateKey } from '@angular/platform-browser';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const RESET = 'RESET';

export const FETCH_TEXT  = '[Home] Fetch Text';
export const FETCH_TEXT_SUCCESS  = '[Home] Fetch Text Success';

export const SET_STATE_FEATURE = '[CORE] Set state feature';

//ACTIONS
export class FetchText implements Action{
  public type: string = FETCH_TEXT;
}

export class FetchTextSuccess implements Action{
  public type: string = FETCH_TEXT_SUCCESS;
  constructor(private payload: any){}
}


export class SetStateFeature implements Action{
  public type: string = SET_STATE_FEATURE;
  constructor(private payload: any){}
}


export interface State{
    name: string;
    sname: string;
    appName: string;
    cat: string;
}

const initialState:State = {
    name: '',
    sname: '',
    appName: '',
    cat: ''
};

export function appReducer(state: State = initialState, action: any) {
  switch (action.type) {

    case RESET:
      return {
        name: 'RESET',
        sname: 'RESET',
        appName: 'RESET'
      };

    case FETCH_TEXT_SUCCESS:
      return {...state, cat: action.payload}
    
    case SET_STATE_FEATURE:
      return {...state, ...action.payload}

    default:
      return state;
  }
}


//Transfer keys
export const CAT_KEY = makeStateKey('cat');