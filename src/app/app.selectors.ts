// reducers.ts
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State as ConfigState } from './app.reducer';


export interface AppState {
  config: ConfigState;
}

export const selectFeature = createFeatureSelector<ConfigState>('config');
export const selectAppName = createSelector(
  selectFeature,
  (state: ConfigState) => state.appName
);
export const selectAppConfig = createSelector(
    selectFeature,
    (state: ConfigState) => {
        return state;
    }
  );

  export const selectCat = createSelector(
    selectFeature,
    (state: ConfigState) => {
        console.log('run cat selector');
        return state.cat
    }
  );