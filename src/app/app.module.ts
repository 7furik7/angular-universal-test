import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule, Inject, PLATFORM_ID, APP_ID, APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';
import { isPlatformBrowser, CommonModule } from '@angular/common';
import { StoreModule, Store } from '@ngrx/store';

import { appReducer } from './app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';
import { AppInitService, appIniProviderFactory } from './app.init';
import { ImagePreloadComponent } from './image-preload/image-preload.component';

const appRoutes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
];



@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ContactComponent,
    ImagePreloadComponent
  ],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'universal-test' }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
    }),

    StoreModule.forRoot({ config: appReducer }),
    RouterModule.forRoot(
      appRoutes, {
        initialNavigation: 'enabled',
        preloadingStrategy: PreloadAllModules
      }
    ),
    EffectsModule.forRoot([AppEffects]),
    BrowserTransferStateModule
  ],
  providers: [
    AppInitService, 
    { provide: APP_INITIALIZER, useFactory: appIniProviderFactory, deps: [AppInitService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }

 }
